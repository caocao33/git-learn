// Import module http
const http = require('http');

// Membuat server HTTP
const server = http.createServer((req, res) => {
  // Menangani permintaan GET pada root path '/'
  if (req.url === '/') {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Halo, dunia!');
  }
  // Menangani permintaan GET pada path '/about'
  else if (req.url === '/about') {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Ini adalah halaman tentang kami.');
  }
  // Menangani permintaan GET pada path yang tidak ditemukan
  else {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('Halaman tidak ditemukan.');
  }
});

// Mendengarkan server pada port 3000
server.listen(3000, () => {
  console.log('Server berjalan pada http://localhost:3000/');
});
